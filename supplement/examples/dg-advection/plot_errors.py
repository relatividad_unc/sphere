"""
Plotting script for convergence rates based on precomputed errors
"""

# Copyright (C) 2013 Marie E. Rognes

import os
import sys
import pylab
import numpy

resultsdir = "results"
resultspath = os.path.join(os.getcwd(), resultsdir)
sys.path.insert(0, resultspath)

# Increase fonts
myfontsize=26
pylab.rc('lines', linewidth=2)
pylab.rc('legend', fontsize=.8*myfontsize)
pylab.rc("lines", markeredgewidth=2.0)
pylab.rc("text", fontsize=.8*myfontsize)

pylab.figure(figsize=(10, 8))

# Import each of the cases here
import errors_DGadvection as case
pylab.loglog(case.h_max, case.L2_error_D, '*-', label="$||D - D_{h}||_0$")
pylab.loglog(case.h_max, numpy.array(case.h_max)**2, '*-', label="$h^2$")
pylab.xlabel("$h$", fontsize=myfontsize)
pylab.ylabel("$L_2$ error", fontsize=myfontsize)
pylab.legend(loc='best')
pylab.grid(True)
pylab.savefig("%s/L2_errors_D.pdf" % resultsdir)

print "The maximum tracer mass conservation error is %.18g"% \
    max(abs(numpy.array(case.conservation_error)))

pylab.show()

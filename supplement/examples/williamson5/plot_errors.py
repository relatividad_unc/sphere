"""
Plotting script for convergence rates based on precomputed values
"""

# Copyright (C) 2013 Marie E. Rognes

import os
import sys
import pylab
import numpy

resultsdir = "results"
resultspath = os.path.join(os.getcwd(), resultsdir)
print "Treating data from %s" % resultspath
sys.path.insert(0, resultspath)

# Increase fonts
myfontsize=26
pylab.rc('lines', linewidth=2)
pylab.rc('legend', fontsize=.7*myfontsize)
pylab.rc('text', fontsize=.8*myfontsize)
pylab.rc("lines", markeredgewidth=2.0)

pylab.figure(figsize=(10, 8))

h_max = [2.27961e6, 1.14449e6, 5.71186e5, 2.85431e5]

RT1 = [0.0119311694739, 0.0072619469351, 0.00226000293643, 0.000630421850074]
BDM1 = [0.00524445210444, 0.0019113394444, 0.00118072918614, 0.000712086495365]
BDFM2 = [0.0044462991246, 0.0013726576569, 0.000423860458933, 0.000143000332865]
BDM2 = [0.00392810326633, 0.00133996047183, 0.000423885720407, 0.000144310410594]

Xa = [2.5e6, 2.5e5]
Yfirst = 9e-9*numpy.power(Xa,1)
Ysecond = 5e-16*numpy.power(Xa,2)

pylab.loglog(h_max, RT1, '^-', color="black", label="RT$_1$")
pylab.loglog(h_max, BDM1, '^-', color="blue", label="BDM$_1$")
pylab.loglog(h_max, BDFM2, '^-', color="red", label="BDFM$_2$")
pylab.loglog(h_max, BDM2, '^-', color="orange", label="BDM$_2$")

pylab.loglog(Xa, Yfirst, color="grey", label=r'$\propto $h$^1$')
pylab.loglog(Xa, Ysecond, color="grey", label=r'$\propto $h$^2$')

pylab.legend(loc='best')
pylab.xlabel("$h$ (m)", fontsize=22)
pylab.ylabel("Normalised $L_2$ error", fontsize=22)
pylab.grid(True)
pylab.savefig("%s/plot1.pdf" % resultsdir)
pylab.show()

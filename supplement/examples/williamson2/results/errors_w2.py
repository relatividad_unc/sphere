# Results from Williamson 2 testcase.  L_2 error norm used.
h_max = [0.0 for i in range(4)];
h_max[0] = 2.27961e+06;
h_max[1] = 1.14449e+06;
h_max[2] = 571186;
h_max[3] = 285431;
uRT1 = [0.0 for i in range(4)];
DRT1 = [0.0 for i in range(4)];
uRT1[0] = 0.0152056;
DRT1[0] = 0.00134645;
uRT1[1] = 0.00368357;
DRT1[1] = 0.000564055;
uRT1[2] = 0.00134659;
DRT1[2] = 0.000189664;
uRT1[3] = 0.000514253;
DRT1[3] = 6.9697e-05;
uBDM1 = [0.0 for i in range(4)];
DBDM1 = [0.0 for i in range(4)];
uBDM1[0] = 0.012578;
DBDM1[0] = 0.00111899;
uBDM1[1] = 0.00243161;
DBDM1[1] = 0.000260996;
uBDM1[2] = 0.000618441;
DBDM1[2] = 7.60771e-05;
uBDM1[3] = 0.000168838;
DBDM1[3] = 2.1574e-05;
uBDFM2 = [0.0 for i in range(4)];
DBDFM2 = [0.0 for i in range(4)];
uBDFM2[0] = 0.0111984;
DBDFM2[0] = 0.0029265;
uBDFM2[1] = 0.00281368;
DBDFM2[1] = 0.000762893;
uBDFM2[2] = 0.000625638;
DBDFM2[2] = 0.000173392;
uBDFM2[3] = 0.000152111;
DBDFM2[3] = 4.38982e-05;
uBDM2 = [0.0 for i in range(4)];
DBDM2 = [0.0 for i in range(4)];
uBDM2[0] = 0.0131137;
DBDM2[0] = 0.00326667;
uBDM2[1] = 0.00244182;
DBDM2[1] = 0.000700245;
uBDM2[2] = 0.000617089;
DBDM2[2] = 0.000150979;
uBDM2[3] = 0.000151871;
DBDM2[3] = 4.30952e-05;

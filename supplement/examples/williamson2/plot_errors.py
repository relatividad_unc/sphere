"""
Plotting script for convergence rates based on precomputed values
"""

# Copyright (C) 2013 Marie E. Rognes

import os
import sys
import pylab
import numpy

resultsdir = "results"
resultspath = os.path.join(os.getcwd(), resultsdir)
print "Treating data from %s" % resultspath
sys.path.insert(0, resultspath)

# Increase fonts
myfontsize=26
pylab.rc('lines', linewidth=2)
pylab.rc('legend', fontsize=.7*myfontsize)
pylab.rc('text', fontsize=.8*myfontsize)
pylab.rc("lines", markeredgewidth=2.0)

pylab.figure(figsize=(10, 8))

# Import each of the cases here
from errors_w2 import h_max, uRT1, uBDM1, uBDFM2, uBDM2, DRT1, DBDM1, DBDFM2, DBDM2

Xa = [3e6, 2.6e5]
Yfirst = 9e-9*numpy.power(Xa,1)
Ysecond = 1.6e-16*numpy.power(Xa,2)

pylab.loglog(h_max, uRT1, 'o-', color="black", label="RT$_1$ velocity")
pylab.loglog(h_max, DRT1, '^-', color="black", label="RT$_1$ depth")

pylab.loglog(h_max, uBDM1, 'o-', color="blue", label="BDM$_1$ velocity")
pylab.loglog(h_max, DBDM1, '^-', color="blue", label="BDM$_1$ depth")

pylab.loglog(h_max, uBDFM2, 'o-', color="red", label="BDFM$_2$ velocity")
pylab.loglog(h_max, DBDFM2, '^-', color="red", label="BDFM$_2$ depth")

pylab.loglog(h_max, uBDM2, 'o-', color="orange", label="BDM$_2$ velocity")
pylab.loglog(h_max, DBDM2, '^-', color="orange", label="BDM$_2$ depth")

pylab.loglog(Xa, Yfirst, color="grey", label=r'$\propto $h$^1$')
pylab.loglog(Xa, Ysecond, color="grey", label=r'$\propto $h$^2$')

pylab.xlim([1e5,1e8])
pylab.xlabel("$h$ (m)", fontsize=22)
pylab.ylabel("Normalised $L_2$ error", fontsize=22)
pylab.legend(loc='best')
pylab.grid(True)
pylab.savefig("%s/plot1.pdf" % resultsdir)
pylab.show()

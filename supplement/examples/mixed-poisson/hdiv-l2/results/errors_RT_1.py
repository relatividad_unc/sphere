# Results from  from H(div) x L^2 mixed-poisson test case.
L2_error_u = [0.0 for i in range(3)];
L2_error_sigma = [0.0 for i in range(3)];
Hdiv_error_sigma = [0.0 for i in range(3)];
h_max = [0.0 for i in range(3)];
h_max[0] = 0.357798;
L2_error_u[0] = 0.00586464;
L2_error_sigma[0] = 0.0197772;
Hdiv_error_sigma[0] = 0.072637;
h_max[1] = 0.179634;
L2_error_u[1] = 0.00302223;
L2_error_sigma[1] = 0.0102239;
Hdiv_error_sigma[1] = 0.0376163;
h_max[2] = 0.0896509;
L2_error_u[2] = 0.00152267;
L2_error_sigma[2] = 0.00515497;
Hdiv_error_sigma[2] = 0.0189771;

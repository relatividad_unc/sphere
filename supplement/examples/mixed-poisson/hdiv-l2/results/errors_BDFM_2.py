# Results from  from H(div) x L^2 mixed-poisson test case.
L2_error_u = [0.0 for i in range(4)];
L2_error_sigma = [0.0 for i in range(4)];
h_max = [0.0 for i in range(4)];
h_max[0] = 0.357798;
L2_error_u[0] = 0.000717646;
L2_error_sigma[0] = 0.00620256;
h_max[1] = 0.179634;
L2_error_u[1] = 0.000184814;
L2_error_sigma[1] = 0.00309335;
h_max[2] = 0.0896509;
L2_error_u[2] = 4.6516e-05;
L2_error_sigma[2] = 0.00154475;
h_max[3] = 0.0448001;
L2_error_u[3] = 1.16474e-05;
L2_error_sigma[3] = 0.000772113;

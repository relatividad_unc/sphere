"""
Plotting script for convergence rates based on precomputed errors
"""

# Copyright (C) 2013 Marie E. Rognes

import os
import sys
import pylab
import numpy

resultsdir = "results"
resultspath = os.path.join(os.getcwd(), resultsdir)
sys.path.insert(0, resultspath)

# Increase fonts
myfontsize=26
pylab.rc('lines', linewidth=2)
pylab.rc('legend', fontsize=.7*myfontsize)
pylab.rc('text', fontsize=.8*myfontsize)
pylab.rc("lines", markeredgewidth=2.0)

pylab.figure(figsize=(10, 8))

# Import each of the cases here
import errors_RT_1 as case
label = "$\mathrm{RT}_1 \\times \, \mathrm{DG}_0$"
pylab.loglog(case.h_max, case.L2_error_u, '*-', label=label)

import errors_BDM_1 as case
label = "$\mathrm{BDM}_1 \\times \, \mathrm{DG}_0$"
pylab.loglog(case.h_max, case.L2_error_u, 'o--', label=label)

import errors_BDFM_2 as case
label = "$\mathrm{BDFM}_2 \\times \, \mathrm{DG}_1$"
pylab.loglog(case.h_max, case.L2_error_u, 'x-', label=label)

import errors_BDM_2 as case
label = "$\mathrm{BDM}_2 \\times \, \mathrm{DG}_1$"
pylab.loglog(case.h_max, case.L2_error_u, 'o--', label=label)

label = "$h$"
pylab.loglog(case.h_max,0.01*numpy.array(case.h_max), '*-', label=label)

label = "$h^2$"
pylab.loglog(case.h_max,0.01*numpy.array(case.h_max)**2, '*-', label=label)

pylab.axis([1.0e-3,1.0,1e-5,1e-2])
pylab.xlabel("$h$", fontsize=myfontsize)
pylab.ylabel("$||u - u_{h}||_0$", fontsize=myfontsize)
pylab.legend(loc='best')
pylab.grid(True)
pylab.savefig("%s/L2_errors_u.pdf" % resultsdir)
pylab.show()

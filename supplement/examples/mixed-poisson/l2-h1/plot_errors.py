"""
Plotting script for convergence rates based on precomputed errors
"""

# Copyright (C) 2013 Marie E. Rognes

import os
import sys
import pylab
import numpy

resultsdir = "results"
resultspath = os.path.join(os.getcwd(), resultsdir)
sys.path.insert(0, resultspath)

# Increase fonts
myfontsize=26
pylab.rc('lines', linewidth=2)
pylab.rc('legend', fontsize=.8*myfontsize)
pylab.rc('text', fontsize=.8*myfontsize)
pylab.rc("lines", markeredgewidth=2.0)

pylab.figure(figsize=(10, 8))
# Import each of the cases here
import errors_DG1xCG2 as case
label = "$||u - u_{h}||_0$"
pylab.loglog(case.h_max, case.L2_error_u, '*-', label=label)

label = "$||u - u_{h}||_1$"
pylab.loglog(case.h_max, case.H1_error_u, '*-', label=label)

pylab.loglog(case.h_max, 0.05*numpy.array(case.h_max), '*-', label="$h$")
pylab.loglog(case.h_max, 0.01*numpy.array(case.h_max)**2, '*-', label="$h^2$")

pylab.xlabel("$h$", fontsize=myfontsize)
pylab.ylabel("Error", fontsize=myfontsize)
pylab.legend(loc='best')

pylab.grid(True)
pylab.savefig("%s/L2_H1_errors_u.pdf" % resultsdir)
#pylab.show()

pylab.figure(figsize=(10, 8))
# Import each of the cases here
import errors_DG0xCG1 as case
label = "$||u - u_{h}||_0$"
pylab.loglog(case.h_max, case.L2_error_u, '*-', label=label)

label = "$||u - u_{h}||_1$"
pylab.loglog(case.h_max, case.H1_error_u, '*-', label=label)
pylab.loglog(case.h_max, 0.05*numpy.array(case.h_max), '*-', label="$h$")
pylab.loglog(case.h_max, 0.01*numpy.array(case.h_max)**2, '*-', label="$h^2$")

pylab.xlabel("$h$", fontsize=myfontsize)
pylab.ylabel("Error", fontsize=myfontsize)
pylab.legend(loc='best')
pylab.grid(True)
pylab.savefig("%s/L2_H1_errors_u_DG0xCG1.pdf" % resultsdir)
pylab.show()

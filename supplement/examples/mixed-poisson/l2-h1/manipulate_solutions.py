"""
Script for playing with and plotting solutions assuming these are
precomputed.
"""

# Copyright (C) 2013 Marie E. Rognes

from dolfin import *
from mixed_poisson_l2_h1 import create_function_spaces

# -----------------------------------------------------------------------------
# Create and initialize application parameters
application_parameters = Parameters("Plotting")
application_parameters.add("mesh", "sphere_ico6")
application_parameters.add("order", 1)
application_parameters.parse()
info(application_parameters, True)

# Define approximation spaces and basis functions
meshid = application_parameters["mesh"]
order = application_parameters["order"]
mesh = Mesh("../hdiv-l2/meshes/%s.xml.gz" % application_parameters["mesh"])
(S, L, V, r) = create_function_spaces(mesh, order)
W = MixedFunctionSpace((S, L, V, r))

filename = "results/w_%s_CG%d.xml.gz" % (meshid, order)
w = Function(W, filename)

(sigma, l, u, r) = w.split(deepcopy=True)

p = plot(u)
p.zoom(1.7)
p.write_png("results/u_%s_CG%d" % (meshid, order))

S = VectorFunctionSpace(mesh, "CG", 1)
s = plot(project(sigma, S))
s.zoom(1.7)
s.write_png("results/sigma_%s_CG%d" % (meshid, order))

interactive()

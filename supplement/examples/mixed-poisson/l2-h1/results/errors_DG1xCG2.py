# Results from  from L^2 x H^1 mixed-poisson test case.
L2_error_u = [0.0 for i in range(4)];
H1_error_u = [0.0 for i in range(4)];
h_max = [0.0 for i in range(4)];
h_max[0] = 0.357798;
L2_error_u[0] = 0.00051919;
H1_error_u[0] = 0.0071428;
h_max[1] = 0.179634;
L2_error_u[1] = 0.000129159;
H1_error_u[1] = 0.0037678;
h_max[2] = 0.0896509;
L2_error_u[2] = 3.21774e-05;
H1_error_u[2] = 0.00190963;
h_max[3] = 0.0448001;
L2_error_u[3] = 8.03549e-06;
H1_error_u[3] = 0.000958034;

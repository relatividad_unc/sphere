# Results from  from L^2 x H^1 mixed-poisson test case.
L2_error_u = [0.0 for i in range(4)];
H1_error_u = [0.0 for i in range(4)];
h_max = [0.0 for i in range(4)];
h_max[0] = 0.357798;
L2_error_u[0] = 0.00311141;
H1_error_u[0] = 0.0212125;
h_max[1] = 0.179634;
L2_error_u[1] = 0.000844747;
H1_error_u[1] = 0.0105752;
h_max[2] = 0.0896509;
L2_error_u[2] = 0.000215529;
H1_error_u[2] = 0.00527507;
h_max[3] = 0.0448001;
L2_error_u[3] = 5.41551e-05;
H1_error_u[3] = 0.0026356;

from dolfin import *

# Define a mesh of a sphere (ball) with radius 1 and a mesh of its
# surface
mesh = Mesh(Sphere(Point(0.0, 0.0, 0.0), 1.0), 8)
surface = BoundaryMesh(mesh, "exterior")

# Integrate 1 over the exterior facets of the mesh of the ball
I = Constant(1.0)
a = I*ds
A = assemble(a, mesh=mesh)

# Integrate 1 over the cells of mesh of the surface of the ball
b = I*dx
B = assemble(b, mesh=surface)

# Confirm that A == B to within numerical precision
eps = 1.e-14
assert (abs(A - B) < eps)

"Test that all code snippets run."

__author__ = "Marie E. Rognes (meg@simula.no) 2013"

import glob
import subprocess

# Run ufl-analyse on each ufl-file found in this directory and
# generate code
ufl_files = glob.glob("*.ufl")
for filename in ufl_files:
    subprocess.call(("ufl-analyse", filename))
    subprocess.call(("ffc", filename))

# Some clean-ups
#subprocess.call(("rm", "*_debug.py", "*_debug.pyc"))

# Run python on each py-file found in this directory
py_files = glob.glob("*.py")
exceptions = ["test.py"]
for filename in py_files:
    if filename in exceptions:
        continue
    subprocess.call(("python", filename))


"""
Generate results required for fenics_sphere_paper. This script is
primarily intended for reproducability, it may be more convenient to
run the separate scripts while playing and testing.

This is expected to run on at least any Ubuntu-like system (assuming
that FEniCS 1.2 is available)

This script does not generate snapshots of solutions.
"""

__author__ = "Marie E. Rognes (meg@simula.no) 2013"

import os
import subprocess

import dolfin # Just to check that dolfin is available

def generate_mixed_poisson_hdiv_l2():
    """Generate data for a series of mixed-poisson H(div) x L^2
    discretizations and generate plot of errors."""

    # Enter correct directory
    current = os.getcwd()
    path = os.path.join(current, "examples", "mixed-poisson", "hdiv-l2")
    print "Entering %s" % path
    os.chdir(path)

    # Generate data (If you have the data already, you can comment out
    # these and just generate the plot if you want to modify the
    # plotting.)
    generate_mixed_poisson_hdiv_l2_data("RT", 1)
    generate_mixed_poisson_hdiv_l2_data("BDM", 1)
    generate_mixed_poisson_hdiv_l2_data("BDFM", 2)
    generate_mixed_poisson_hdiv_l2_data("BDM", 2)

    # Generate plot of convergence
    filename = "plot_errors.py"
    cmd = ("python", filename)
    print "Calling %s" % " ".join(cmd)
    subprocess.call(cmd)

    # Return to current
    os.chdir(current)

def generate_mixed_poisson_hdiv_l2_data(family, order):
    """Generate data for mixed-poisson H(div) x L^2 formulation for a
    given family and order."""

    filename = "mixed-poisson-sphere.py"
    cmd = ("python", filename, "--family", family, "--order", str(order))
    print "Calling %s" % " ".join(cmd)
    subprocess.call(cmd)

def generate_mixed_poisson_l2_h1():
    """Generate data for a series of mixed-poisson L^2 x H^1
    discretizations and generate plot of errors."""

    # Enter correct directory
    current = os.getcwd()
    path = os.path.join(current, "examples", "mixed-poisson", "l2-h1")
    print "Entering %s" % path
    os.chdir(path)

    # Generate data (If you have the data already, you can comment out
    # these and just generate the plot if you want to modify the
    # plotting.)
    generate_mixed_poisson_l2_h1_data(0)
    generate_mixed_poisson_l2_h1_data(1)

    # Generate plot of convergence
    filename = "plot_errors.py"
    cmd = ("python", filename)
    print "Calling %s" % " ".join(cmd)
    subprocess.call(cmd)

    # Return to current
    os.chdir(current)

def generate_mixed_poisson_l2_h1_data(order):
    """Generate data for mixed-poisson L^2 x H^1 formulation for a
    given order."""

    # Note that this can take a little time actually!

    filename = "mixed_poisson_l2_h1.py"
    cmd = ("python", filename, "--order", str(order))
    print "Calling %s" % " ".join(cmd)
    subprocess.call(cmd)

def generate_linear_shallow_water():

    # Enter correct directory
    current = os.getcwd()
    path = os.path.join(current, "examples", "linear-shallow-water")
    print "Entering %s" % path
    os.chdir(path)

    # Generate data (If you have the data already, you can comment out
    # these and just generate the plot if you want to modify the
    # plotting.)
    filename = "linear_shallow_water.py"
    cmd = ("python", filename)
    print "Calling %s" % " ".join(cmd)
    subprocess.call(cmd)

    # Generate plot of energies
    filename = "plot_energies.py"
    results_dir = os.path.join("results", "RT1_sphere_ico5")
    cmd = ("python", filename, results_dir)
    print "Calling %s" % " ".join(cmd)
    subprocess.call(cmd)

    # Return to current
    os.chdir(current)

def generate_dg_advection():
    """Generate data and plots for the DG_1 advection formulation."""

    # Enter correct directory
    current = os.getcwd()
    path = os.path.join(current, "examples", "dg-advection")
    print "Entering %s" % path
    os.chdir(path)

    # Run script to generate data
    filename = "dg-advection.py"
    cmd = ("python", filename)
    print "Calling %s" % " ".join(cmd)
    subprocess.call(cmd)

    # Generate plot of energies
    filename = "plot_errors.py"
    cmd = ("python", filename)
    print "Calling %s" % " ".join(cmd)
    subprocess.call(cmd)
    
def generate_williamson2():
    """Generate data for the Williamson 2 test case."""

    # Enter correct directory
    current = os.getcwd()
    path = os.path.join(current, "examples", "williamson2")
    print "Entering %s" % path
    os.chdir(path)

    # Run script to generate data
    filename = "manual.py"
    cmd = ("python", filename)
    print "Calling %s" % " ".join(cmd)
    subprocess.call(cmd)

    # Generate plot (pre-existing data)
    filename = "plot_errors.py"
    cmd = ("python", filename)
    print "Calling %s" % " ".join(cmd)
    subprocess.call(cmd)
    
def generate_williamson5():
    """Generate data for the Williamson 2 test case."""

    # Enter correct directory
    current = os.getcwd()
    path = os.path.join(current, "examples", "williamson5")
    print "Entering %s" % path
    os.chdir(path)

    # Run script to generate data
    filename = "w5.py"
    cmd = ("python", filename)
    print "Calling %s" % " ".join(cmd)
    subprocess.call(cmd)

    # Generate plot (pre-existing data)
    filename = "plot_errors.py"
    cmd = ("python", filename)
    print "Calling %s" % " ".join(cmd)
    subprocess.call(cmd)

def main():

    # Comment/uncomment at will to generate results and figures for
    # different numerical experiments

    pass
    generate_mixed_poisson_hdiv_l2()
    generate_mixed_poisson_l2_h1()
    generate_linear_shallow_water()
    generate_dg_advection()
    generate_williamson2()
    generate_williamson5()

if __name__ == "__main__":

    main()

